<?php require_once "./code.php"?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Activity 4</title>
</head>
<body>
    <h1>Activity</h1>

    <h1>Building</h1>
    <p>The name of the building is <?php echo $building->getName(); ?>.</p>
    <p>The <?php echo $building->getName();?> has <?php echo $building->getFloors(); ?> floors.</p>
    <p>The <?php echo $building->getName();?> is located at <?php echo $building->getAddress(); ?>.</p>
    <?php $building->setName('Caswynn Complex'); ?>
    <p>The name of the building has been changed to <?php echo $building->getName(); ?>.</p>

    <h1>Condominium</h1>
    <p>The name of the condominium is <?php echo $condominium->getName(); ?></p>
    <p>The <?php echo $condominium->getName();?> has <?php echo $condominium->getFloors(); ?> floors.</p>
    <p>The <?php echo $condominium->getName();?> is located at <?php echo $condominium->getAddress(); ?>.</p>
    <?php $condominium->setName('Enzo Tower'); ?>
    <p>The name of the condominium has been changed to <?php echo $condominium->getName(); ?></p>
    
</body>
</html>